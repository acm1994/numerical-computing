% Aaron Christopher Montano
% Melissa Moreno
% 02 / 27 / 2018
% CS / MATH 375 - Homework 4
% Question 2.

% Global variables.
cpuSum = 0; % Will be summed up as the total number of cpu times when finding the average.
cpuAvg = 0; % cputime average for a given problem.
numTests = 100; % Total number of tests.
start = 0; % Starting flag. (cputime)
stop = 0; % Ending flag. (cputime)
cpuTimes = zeros(1, 4); % Array of all computed cputimes. 
N = [261, 399, 561, 1592]; % Size of matrix. 

% eiffel1.
load('eiffel1.mat')
[m, n] = size(A);
x = zeros(m, 1);
b = zeros(m, 1);
b(1) = 1/5; % Apply the same force used in the first question.
for i = [1:100]
   start = cputime;
   x = A\b;
   cpuSum = cpuSum + (cputime - start);
end
cpuAvg = cpuSum / numTests; % Compute cputime average.
cpuTimes(1) = cpuAvg;
cpuSum = 0; % Reset cputime. 

% eiffel2.
load('eiffel2.mat')
[m, n] = size(A);
x = zeros(m, 1);
b = zeros(m, 1);
b(1) = 1/5;
for i = [1:100]
   start = cputime;
   x = A\b;
   cpuSum = cpuSum + (cputime - start);
end
cpuAvg = cpuSum / numTests;
cpuTimes(2) = cpuAvg;
cpuSum = 0;

% eiffel3.
load('eiffel3.mat')
[m, n] = size(A);
x = zeros(m, 1);
b = zeros(m, 1);
b(1) = 1/5;
for i = [1:100]
   start = cputime;
   x = A\b;
   cpuSum = cpuSum + (cputime - start);
end
cpuAvg = cpuSum / numTests;
cpuTimes(3) = cpuAvg;
cpuSum = 0;

% eiffel4.
load('eiffel4.mat')
[m, n] = size(A);
x = zeros(m, 1);
b = zeros(m, 1);
b(1) = 1/5;
for i = [1:100]
   start = cputime;
   x = A\b;
   cpuSum = cpuSum + (cputime - start);
end
cpuAvg = cpuSum / numTests;
cpuTimes(4) = cpuAvg;
cpuSum = 0;

% Plot.
fig = figure;
loglog(N, cpuTimes, 'r-')
hold on
loglog(N, N.^3 - 99999998, 'b--')
xlabel('N')
ylabel('cpuTime')
grid on
legend('cputime', 'x^3')
print(fig, 'question2', '-dpng')
