% Aaron Christopher Montano
% Melissa Moreno
% 02 / 26 / 2017
% Homework 4: Question 1

% Load the first .mat file and plot the unloaded tower.
load('eiffel1.mat')
fig = figure;
trussplot(xnod, ynod, bars)

% Solve A\b and compute new loads.
[m, n] = size(A);
b = zeros(m, 1);
x = zeros(m, 1);
b(5) = 1/5;
x = A\b;
xload = xnod + x(1:2:end);
yload = ynod + x(2:2:end);

% New plot.
grid on
xlabel('X')
ylabel('Y')
trussplot(xload, yload, bars, '*')
print(fig, 'question1', '-dpng')
