% Aaron Christopher Montano
% Melissa Moreno
% 02 / 27 / 2018
% CS / MATH 375: Homework 4
% Question 3(a)

% Initialize global variables.
numEiff = ['1', '2', '3', '4'];
fileNameIn = '';
fileNameOut = '';
start = 0;

for i = numEiff
  
  % Open file and reset variables. 
  fileName = strcat('eiffel', i, '.mat');
  load(fileName)

  % Plot original tower.
  %fig = figure;
  %trussplot(xnod, ynod, bars)
  
  cpuSum = 0;
  maxDisp = 0;
  xNorm = 0;
  [m, n] = size(A);
  A = sparse(A);
  spy(A)
  x = zeros(m, 1);
  b = zeros(m, 1);
  bMax = b; % Force vector that results in the maximum displacement. 
  xMax = x; % Maximum displacement vector.
  for i = [1:2:m]
    b(i) = 1/5;
    start = cputime;
    x = A\b;
    cpuSum = cpuSum + (cputime - start);

    % Determine if the vector x has the greatest displacement.
    xNorm = norm(x, inf);
    if xNorm > maxDisp
      maxDisp = xNorm;
      xMax = x;
      bMax = b;
    end
    b(i) = 0;
  end

  % Report.
    fprintf('cpuTime (N = %d): %d \n', m, cpuSum)
  fprintf('Maximum displacement (N = %d): %d \n', m, maxDisp)

  % Plot.
  %xload = xnod + xMax(1:2:end);
  %yload = ynod + xMax(2:2:end);
  %trussplot(xload, yload, bars, '*')
  %hold off
  %xlabel('X')
  %ylabel('Y')
  %fileNameOut = strcat('problem3a', i);
  %print(fig, fileNameOut, '-dpng')
	  
end
