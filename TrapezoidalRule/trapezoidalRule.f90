  ! Aaron Christopehr Montano
  ! 04 / 24 / 2018
  ! Trapezoidal Rule.
program trapzoidalRule
  implicit none

  ! Declare variables.
  double precision :: a, b, h, summ, dx
  integer :: i

  ! Read in boundaries and compute the
  ! discretization value.
  print *, "Enter boundary values (a, b) and &
       &number of discretization points."
  read *, a, b, h
  dx = (b-a)/h

  ! Carry out Trapezoidal Rule.
  summ = (f(a) + f(b))/2
  do i = 1, int(h-1)
    summ = summ + 2*f(a + dx*i)
  end do
  print *, "Approximated integral: ", summ*(b-a)/h
  
contains

  double precision function f(x)
    implicit none

    ! Declare variables.
    double precision, intent(in) :: x

    ! Computation.
    f = x
  end function f

  

end program trapzoidalRule

