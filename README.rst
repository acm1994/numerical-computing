************************************************************
README file for Math 471, Fall 2017, J.Murrietta & A.Montano
************************************************************

- This repository will contain all homeworks and projects for 
  UNM Math 471.
- This repository will be updated as more homeworks assigned
  to provide needed information.
- Homework 1 can be located in the HW1 directory. It contains
  a README file talking about the assignment. Assignment involved
  setting up git and updating a test.txt file. Instructions to find
  are in the README file in the HW1 directory. No code/report
  was needed for this assignment.
- Homework 2 can be located in the hw2 directory.
- Homework 3 is located in the hw3 directory.
- Homework 4 is located in the hw4 directory.
