% Fixed-point iteration.
% fixedPoint(x0, g, n, p)
% Param:
% x0 - initial guess.
% g - function.
% n - limit to the number of iterations. 
% p - correct to 'p' decimal points.
function r = fixedPoint(x0, g, n, p)

   % Define variables.
   iter = 0;
   tol = 0.5*10^(-p);
   xi = 0;
   xPrev = 0;

   % Evaluate g(x) at initial guess.
   xPrev = x0;
   xi = g(xPrev);

   % Begin while loop. Will terminate when
   % abs(xi - xPrev) < tol.
   while abs(xi - xPrev) >= tol

     % Check to see if we have gone past our iteration
     % limit.
     if iter > n
       error('Fixed-Point Iteration failure; divergence.')
       return
     end  

     % Update.
     xPrev = xi;
     xi = g(xPrev);
     iter = iter + 1;

   end

   fprintf('Fixed-Point Iteration convergence after %d iterations.\n', iter)
   r = xi;

end
