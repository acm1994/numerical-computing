% CS / MATH 375
% 02 / 10 / 2017
% Problem 1 (c)

% Define input parameters.
a = 0;
b = pi;
f = @(x) 2*x - 1 - sin(x);
p = 8;

% Call.
bisect(a, b, f, p)
