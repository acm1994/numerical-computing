% Newton's Method for finding roots of a function.
% newtonMethod(x0, f, fP, n, p)
% Param:
% x0 - initial guess.
% f - function (passed as anonymous function).
% fP - derivative of f.
% n - iteration limit.
% p - correct to 'p' decimal points.
function r = newtonMethod(x0, f, fP, n, p)

   % Define variables.
   iter = 0;
   tol = 0.5*10^(-p);
   xi = 0;
   xPrev = 0;

   % Evaluate initial guess.
   xPrev = x0;
   xi = xPrev - f(xPrev) / fP(xPrev);

   % Loop.
   while abs(xi - xPrev) > tol 

     % Check to see if we are past our iteration limit.
     if iter > n
       error('Newtons Method failure; divergence.')
       return
     end

     % Updates.
     xPrev = xi;
     xi = xPrev - f(xPrev) / fP(xPrev);
     iter = iter + 1;

   end 

   % Return final values.
   fprintf('Newton Method convergence after %d iterations.\n', iter)
   r = xi;

end 
