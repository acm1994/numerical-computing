% Bisection Method for finding function roots.
% bisect(a, b, f, p)
% Param:
% a - left-hand bound.
% b - right-hand bound.
% p - tolerance (i.e., correct to 'p' decimal points).
% f - function handle (e.g., @sin). 
function r = bisect(a, b, f, p)

    % Define constants.
    tol = 0.5*10^(-p); % Tolerance (used as stoppoing criteria)
    del = 0; % delta (used as checking error) 
    iter = 0; % Number of iterations (will return at end).

    % First, we are going to check to see if f(a)*f(b) < 0. If so,
    % by the IVT, the interval has a root. Otherwise, return an error
    % message.
    if f(a)*f(b) > 0
        error('Root not found in interval. ')
    else

    % Otherwise, we take the midpoint of the interval and 
    % repeat the iterations. 
    c = (a + b) / 2;
    del = (b - a) / 2;

    % Main loop.
    while del >= tol

        % Perform initial test.
        if f(a)*f(c) < 0
            b = c;
        else
            a = c;
        end 

        % Update.
        c = (a + b) / 2;
        del = (b - a) / 2;
        iter = iter + 1;

    end 

    % Return final output.
    fprintf('\nBisection Method used %d iterations.', iter)
    r = c;

    end

end 
