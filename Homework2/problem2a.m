% CS / MATH 375
% 02 / 12 / 2017
% Problem 2 (c)

% Define domain & function.
x = [-2:0.05:10];
y = 2*x - 1 - sin(x);

% Plot.
plot(x, y, 'r-')
grid on
xlabel('X', 'fontsize', 16)
ylabel('Y', 'fontsize', 16)
title('2x - 1 - sin(x)', 'fontsize', 20)
axis([-2, 10, -2, 6])
print -depsc2 plot.png
