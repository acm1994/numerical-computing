% Secant Method for finding roots of a function.
% secantMethod(x0, x1, f, n, p)
% Param:
% x0 - initial guess (0).
% x1 - initial guess (1).
% f - function (passed as anonymous function).
% n - iteration limit.
% p - correct to 'p' decimal points.
function r = secantMethod(x0, x1, f, n, p)

   % Define variables.
   iter = 0;
   tol = 0.5*10^(-p);
   xi = x1;
   xPrev = x0;

   % Evaluate initial guess.
   xPprev = 
   xi = xPrev - f(xPrev) / fP(xPrev);

   % Loop.
   while abs(xi - xPrev) > tol 

     % Check to see if we are past our iteration limit.
     if iter > n
       error('Secant Method failure; divergence.')
       return
     end 
   
     % Updates.
     xPrev = xi;
     xi = xPrev - f(xPrev) / fP(xPrev);
     iter = iter + 1;

   end 

   % Return final values.
   fprintf('Secant Method convergence after %d iterations.\n', iter)
   r = xi

end 
