! Aaron Christopher Montano
! 06/12/2018
! Root Finding: Bisection Method
program bisect
  implicit none

  ! Interface.
  interface
     ! Function of interest.
     real(kind = 8) function f(x)
       real(kind = 8), intent(in) :: x
     end function f
  end interface

  ! Declare variables.
  real(kind = 8) :: tol, del, c, a, b
  integer :: iter, n

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! Determine interval.
  a = -3
  b = 4
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  ! Correct to 8 decimal points.
  tol = 0.5*10**(-8)
  del = 0;
  iter = 0;

  ! Check to see if the original interval in question contains
  ! a root.
  if (f(a)*f(b) > 0) then
     write(*, *) "Bisection Method error: Root not found in interval."
  else
     ! If a root does exist, then implement method on the midpoint.
     c = (a+b)/2
     del = (b-a)/2

     ! Main while loop.
     do while (del >= tol)
        ! Initial test.
        if (f(a)*f(b) < 0) then
           b = c
        else
           a = c
        end if

        ! Update.
        c = (a+b)/2
        del = (b-a)/2
        iter = iter + 1
     end do
  end if

  ! Return final output.
  write(*, *) "Bisection Method convergence: ", iter, " iterations."
  write(*, *) "r = ", c
  
end program bisect

real(kind = 8) function f(x)
  implicit none
  real(kind = 8), intent(in) :: x
  f = 2*x-1-sin(x)
end function f

