! Root-Finding: Newton's Method.
program newton
  implicit none

  ! Program interface.
  interface
     ! Main function.
     real(kind = 8) function f(x)
       real(kind = 8), intent(in) :: x
     end function f

     ! Function derivative.
     real(kind = 8) function fp(x)
       real(kind = 8), intent(in) :: x
     end function fp
  end interface

  ! Initialization of variables.
  integer :: iter, n
  real(kind = 8) :: tol, xi, xPrev, x0, p

  !! Initial guess.
  x0 = 5

  ! Initial variables.
  iter = 0
  n = 1000
  p = 8
  tol = 0.5*10**(-p)
  xi = 0
  xPrev = 0

  ! Evaluate initial guess.
  xPrev = x0
  xi = xPrev - f(xPrev)/fp(xPrev)

  ! Main loop.
  ! Continue for as long as abs(xi-xPrev) > tol.
  do while (abs(xi-xPrev) > tol)
     ! Check to see if we are past our iteration limit.
     if (iter > n) then
        exit
     else
        ! Otherwise, continue with computations.
        ! Update.
        xPrev = xi
        xi = xPrev - f(xPrev)/fp(xPrev)
        iter = iter + 1
     end if
  end do

  ! If we went past our iteration limit, return an error message.
  if (iter > n) then
     write(*, *) "Newton's Method failure; divergence."
  else
     write(*, *) "Newton's Method convergence at ", iter, "iterations."
     write(*, *) "r = ", xi
  end if
  
end program newton

! Main function.
real(kind = 8) function f(x)
  implicit none

  real(kind = 8), intent(in) :: x

  ! Return.
  f = x**3
end function f

! Function derivative.
real(kind = 8) function fp(x)
  implicit none

  ! Variable declaration.
  real(kind = 8), intent(in) :: x

  ! Return.
  fp = 3*x**2
end function fp

