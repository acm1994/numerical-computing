! Root-Finding: Fixed-Point Iteration
program fixedPoint
  implicit none

  ! Program interface.
  interface
     real(kind = 8) function g(x)
       real(kind = 8), intent(in) :: x
     end function g
  end interface

  ! Declare variables.
  integer :: iter, n
  real(kind = 8) :: tol, p, xi, xPrev, x0

  ! Initialize variables.
  iter = 0
  n = 1000
  p = 8
  tol = 0.5*10**(-p)
  x0 = 1.1

  ! Initial guess.
  xPrev = x0
  xi = g(xPrev)

  ! Commence main while loop; terminate when
  ! abs(xi - xPrev) <= tol
  do while (abs(xi-xPrev) > tol)
     ! If we are past our iteration limit, exit loop.
     if (iter > n) then
        exit
     else
        ! Otherwise, continue with computations.
        ! Update.
        xPrev = xi
        xi = g(xPrev)
        iter = iter+1
     end if
  end do

  ! If we went past our iteration limit, return an error.
  if (iter > n) then
     write(*, *) "Fixed-point iteration error: past iteration limit."
  else
     write(*, *) "FPI convergence after ", iter, " iterations."
     write(*, *) "r = ", xi
  end if

end program fixedPoint

real(kind = 8) function g(x)
  implicit none

  ! Declaration of variables.
  real(kind = 8), intent(in) :: x

  ! Return value.
  g = 0.5*sin(x)
end function g

