% Set up important variables.
N = [10, 20, 40, 80, 160, 320, 640, 1280];
means = [];
k = 1;

% For loop.
for n = N
    means(k) = my_mean(@my_fun, -1, 1, n);
    k = k + 1;
end
means

% Exact value.
M = 1/exp(1);

% Compute and plot absolute error.
abs_err = abs(means - M);

%semilogy( N, abs_err)
%xlabel('N', 'fontsize', 14)
%ylabel('Abs(err)', 'fontsize', 14)
%grid on
%legend('abs. error')

fprintf('\\begin{tabu} to 0.8\\textwidth { | X[l] | X[c] | X[r] | }\n\\hline\n')
fprintf('N & approx. & abs_err\n\\hline\n')
for i = [1:(length(N))]
    fprintf('%d & %d & %d\n\\hline\n', N(i), means(i), abs_err(i))
end
fprintf('\\end{tabu}\n')
