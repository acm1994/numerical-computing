% Declare vectors.
x = [0:(pi/1e8):pi];
y = cos(x);

% Vectorized implementation.
tic
dot(x, y)
toc
  
