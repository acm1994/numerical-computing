function result = my_mean(fun, a, b, N)

    % Test a<= b.
    if a <= b
        
        % Set up spacing.
        h = (b - a) / N;
  
        % Set up function.
        f = fun;
        
        % Perform summation.
        summ = 0;
        for j = [1:N]
            summ = summ + f(a + (j-1)*h);
        end
        
        % Return value.
        result = summ / N;
     
    else
        
        error('Error: a must be smaller than b.')
        
    end 

end 