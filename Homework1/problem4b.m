% Declare vectors.
x = [0:(pi/1e8):pi];
y = cos(x);

% Implement 'sum' command.
tic
z = x.*y;
sum(z)
toc
