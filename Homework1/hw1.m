clc
clear
close ALL

% Aaron Christopher Montano
% 01 / 21 / 2017
% MATH 375: Homework 1 - MATLAB Review

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% Problem 1. %%%%%

z = [10 40 50 80 30 70 60 90];

fprintf('(i)')
z(z >= 50)

fprintf('(ii)')
z(1:2:7) = zeros(1, 4)
