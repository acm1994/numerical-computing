% Vandermonde Interpolation (new coordinates).
% Vandermonde polynomial evaluation with new data points.
% Param:
% a - polynomial coefficients (as determined by interpvan).
% xnew - set of new x-coordinates.
% NOTE: vectors "a" and "xnew" must be the same dimension.
function ynew = vaninterp(a, xnew)

  % Generate vectors and evaluate points.
  a = a';
  y = [];
  exps = [(length(a) - 1):-1:0];
  for i = [1:length(xnew)]
    y(i) = sum(a.*xnew(i).^exps);   
  end

  % Return y-values.
  ynew = y;

end
