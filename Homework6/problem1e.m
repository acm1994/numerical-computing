% Aaron Christopher Montano
% Melissa Moreno
% MATH / CS 375: Homework 6 - Problem 1 (e)

% Define data points and interpolate.
x = [-1, 0, 2, 3, 5, 6, 8];
y = [-1, 2, 0, 5, 6, 9, 12];
xnew = linspace(-1.5, 8.5, 1000);
ynew = newtoninterp(x, y, xnew);

% Plot.
fig = figure;
plot(x, y, 'r.', 'MarkerSize', 20)
hold on
grid on
plot(xnew, ynew, 'b-')
xlabel('x')
ylabel('y')
print(fig, 'problem1e', '-dpng')

% Compute residual.
xnew = x;
ynew = newtoninterp(x, y, xnew);
format long
r = norm(ynew - y, 2)
