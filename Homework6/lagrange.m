% Polynomial Interpolation (Lagrange).
% Interpolates a polynomial given a set of data points via Lagrange method.
% Param:
% x - x-coordinates (row vector).
% y - y-coordinates (row vector).
% xnew - new x-coordintes (row vector).
function ynew = lagrange(x, y, xnew)

  % Anonymous function and n.
  lag = @(xj, xk, xi) prod((xj-xk)./(xi-xk));
  n = length(x);

  % Check to see if the input vectors x and y are the same length.
  if length(x) ~= length(y)
    error('Lagrange interpolation error: input vectors must be the same length.') 
  end

  % Evaluate.
  L = [];
  yTemp = [];
  for j = [1:length(xnew)]
    for i = [1:n]
      L(i) = lag(xnew(j), x(x~=x(i)), x(i));  
    end
    yTemp(j) = sum(y.*L);
  end

  % Return.
  ynew = yTemp;
      
end 
