% Polynomial Interpolation (Newton's Method) 
% Interpolates a polynomial given a set of data points (x, y).
% param: 
% x - x-values (vector)
% y - y-values (vector)
% xnew - set of new x-values to be evaluated with the new polynomial.
% NOTE 1: all input vectors must be row vectors. 
% NOTE 2: input vectors x and y must be the same length.
% All code was based off of the MATLAB script from:
% https://math.okstate.edu/people/yqwang/teaching/math4513_fall12/Notes/NewtonsDivDiff.html
function ynew = newtoninterp(x, y, xnew)

  % Check to see if input vectors x and y are the same length. 
  if length(x) ~= length(y)
    error('Newton Divided Differences error: input vectors x and y must be the same length.')
  end

  % Generate a divided differences table. 
  n = length(x)-1;
  divDiffs = zeros(n+1, n+1);
  divDiffs(:, 1) = y;

  % Perform divided differences and retrieve coefficients. 
  for i=1:n
    for j=1:i
        divDiffs(i+1,j+1) = (divDiffs(i+1,j) - divDiffs(i,j)) / (x(i+1)-x(i-j+1));
    end
  end
  coeff = diag(divDiffs);

  ynew = coeff;

  % Evaluate polynimial (with new coefficients) with the nested formula. 
  yTemp = [];
  for i = [1:length(xnew)]
    accum = coeff(n+1);
    for k = [n:-1:1]
      accum = coeff(k) + (xnew(i)-x(k))*accum;
    end
    yTemp(i) = accum;
  end

  % Return values.
  ynew = yTemp;

end 
