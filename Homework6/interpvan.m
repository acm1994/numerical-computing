% Polynomial Interpolation (Vandermonde)
% Interpolates a polynomial given a set of data points (x, y).
% param:
% x - x-values (vector).
% y - y-values (vector).
% NOTE: input vector y must be a row vector.
function a = interpvan(x, y)

  % Check if x and y are the same dimensions.
  if length(x) ~= length(y)
    error('Vandermonde Method failure: vectors must be the same dimensions. ')
  end

  % Set up system of equations.
  m = length(x);
  y = y';
  mults = [(m-1):-1:0];
  V = zeros(m, m);
  for i = [1:m]
    V(i, :) = x(i).^mults;
  end 

  % Solve.
  coeff = (V\y);
  a = coeff;

end 

  
