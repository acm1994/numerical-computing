% Aaron Christopher Montano
% Melissa Moreno
% CS / MATH 375: Homework 6 - Problem 1 (d)

% Define data points.
x = [-1, 0, 2, 3, 5, 6, 8];
y = [-1, 2, 0, 5, 6, 9, 12];

% Find coefficients of interpolating polynomial.
a = interpvan(x, y);

% Plot new function on n = 7 data points.
n = 7;
xnew = linspace(-1.5, 8.5, 1000);
ynew = vaninterp(a, xnew);
fig = figure(1)
plot(x, y, 'k.', 'MarkerSize', 20)
hold on
grid on
plot(xnew, ynew, 'b-')
xlabel('x')
ylabel('y')
print(fig, 'problem1d', '-dpng') 

% Find condition number of v7.
v7 = [];
mults = [(length(x)-1):-1:0];
for i = [1:length(x)]
  v7(i, :) = x(i).^mults;
end
format long 
condNum = cond(v7, inf)

% Compute the 2-norm of the residual.
r = norm(v7*a - y', 2)
