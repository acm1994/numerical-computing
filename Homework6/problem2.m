% Aaron Christopher Montano
% Melissa Moreno
% MATH / CS 375: Homework 6 - Question 2

f = @(x) 1./(1+16.*x.^2);

%% Equally spaced: n = 10.
% Define data points.
n = 10;
x = [1:n];
x = -1+2.*(x-1)./(n-1);
y = f(x);
xnew = linspace(-1, 1, 10000);
ynew = lagrange(x, y, xnew);
fig1 = figure;
plot(x, y, 'r.', 'MarkerSize', 20)
hold on
grid on
plot(xnew, ynew, 'b-')
xlabel('x')
ylabel('y')
axis([-1, 1, -.5, 1.5])
print(fig1, 'eqsp10', '-dpng')

%% Equally spaced: n = 20.
n = 20;
x = [1:n];
x = -1+2.*(x-1)./(n-1);
y = f(x);
xnew = linspace(-1, 1, 10000);
ynew = lagrange(x, y, xnew);
fig2 = figure;
plot(x, y, 'r.', 'MarkerSize', 20)
hold on
grid on
plot(xnew, ynew, 'b-')
xlabel('x')
ylabel('y')
axis([-1, 1, -.5, 1.5])
print(fig2, 'eqsp20', '-dpng')

%% Equally spaced: n = 40.
n = 40;
x = [1:n];
x = -1+2.*(x-1)./(n-1);
y = f(x);
xnew = linspace(-1, 1, 10000);
ynew = lagrange(x, y, xnew);
fig3 = figure;
plot(x, y, 'r.', 'MarkerSize', 20)
hold on
grid on
plot(xnew, ynew, 'b-')
xlabel('x')
ylabel('y')
axis([-1, 1, -.5, 1.5])
print(fig3, 'eqsp40', '-dpng')

%% Chebyshev: n = 10.
n = 10;
x = [1:n];
x = cos(((2.*x - 1).*pi)./(2.*n));
y = f(x);
xnew = linspace(-1, 1, 10000);
ynew = lagrange(x, y, xnew);
fig4 = figure;
plot(x, y, 'r.', 'MarkerSize', 20)
hold on
grid on
plot(xnew, ynew, 'b-')
xlabel('x')
ylabel('y')
axis([-1, 1, -.5, 1.5])
print(fig4, 'chsp10', '-dpng')

%% Chebyshev: n = 20.
n = 20;
x = [1:n];
x = cos(((2.*x-1).*pi)./(2.*n));
y = f(x);
xnew = linspace(-1, 1, 10000);
ynew = lagrange(x, y, xnew);
fig5 = figure;
plot(x, y, 'r.', 'MarkerSize', 20)
hold on
grid on
plot(xnew, ynew, 'b-')
xlabel('x')
ylabel('y')
axis([-1, 1, -.5, 1.5])
print(fig5, 'chsp20', '-dpng')

%% Chebyshev: n = 40.
n = 40;
x = [1:n];
x = cos(((2.*x-1).*pi)./(2.*n));
y = f(x);
xnew = linspace(-1, 1, 10000);
ynew = lagrange(x, y, xnew);
fig6 = figure;
plot(x, y, 'r.', 'MarkerSize', 20)
hold on
grid on
plot(xnew, ynew, 'b-')
xlabel('x')
ylabel('y')
axis([-1, 1, -.5, 1.5])
print(fig6, 'ch40', '-dpng')
