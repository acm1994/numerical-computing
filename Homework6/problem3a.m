% Aaron Christopher Montano
% Melissa Moreno
% MATH / CS 375: Homework 6 - Problem 3 (a)

% Retrive data points.
x = [-1, 2, 5, 3, 6, 2.4, 3.8, 7.1, 8.5, 3.3];
y = [1, 5, 3, 2, 3.2, 5.2, 1.2, 6.8, 9.9, 7.2];
xnew = linspace(-1, 8.5, 10000);
ynew = newtoninterp(x, y, xnew);
fig = figure;
plot(x, y, 'r.', 'MarkerSize', 20)
hold on
grid on
plot(xnew, ynew, 'b-')
xlabel('x')
ylabel('y')
print(fig, 'problem3aFirst', '-dpng')

% Spline.
cs = spline(x, [0 y 0]);
xx = linspace(-1, 8.5, 10000);
fig2 = figure;
plot(x, y, 'r.', 'MarkerSize', 20)
hold on
grid on
plot(xx, ppval(cs, xx), 'b-')
xlabel('x')
ylabel('y')
print(fig2, 'problem3Second', '-dpng')
