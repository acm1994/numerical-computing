  ! Aaron Christopher Montano
  ! 04 / 24 / 2018
  ! Midpoint Integral Approximation.
program midpoint
  implicit none

  ! Declare variables.
  double precision :: a, b, m, summ, h, wi
  integer :: i

  ! Read in variables.
  print *, "Enter boundary values (a, b) and&
       & number of sub-intervals."
  read *, a, b, m
  h = (b-a)/m

  ! Carry out midpoint rule.
  summ = 0
  do i = 1, int(m)
     wi = (i-1)*h + h/2
     summ = summ + h*f(wi)
  end do

  ! Return values.
  print *, summ

contains

  double precision function f(x)
    implicit none 

    ! Declare variables.
    double precision, intent(in) :: x

    ! Return evaluated function value.
    f = x
  end function f

end program midpoint

