% Aaron Christopher Montano
% Melissa Moreno
% MATH / CS 375: Homework 3
% Approximation of f'(0), f(x) = exp(x).

a = 0;
<<<<<<< HEAD
h = logspace(-1, -16, 100);
y = abs(1 - ((exp(a + h) - exp(a - h)) ./ (2.*h)));
=======
h = linspace(-1, -16, 100);
y = abs((exp(a + h) - exp(a - h)) ./ (2.*h) - 1);
>>>>>>> 3770023fdaff1cb684cac6023e841b6d49193d81
y2 = h.^2;
fig = figure;
loglog(h, y, 'b-')
hold on
loglog(h, y2, 'r--')
grid on
legend('Err.', 'h^2', 'Location', 'southeast')
xlabel('log(h)')
ylabel('Approximation Error')
print(fig,'MySavedPlotTest','-dpng')
