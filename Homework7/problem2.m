% Aaron Christopher Montano
% Melissa Moreno
% MATH / CS 375: Homework 7 - Problem 2
% 04 / 21 / 2018

% Load and plot the data points.
load data2.mat
fig1 = figure(1);
set(gca, 'fontsize', 20)
plot(x, c, 'ro')
xlabel('x')
ylabel('c')
print(fig1, 'dataPlot2', '-dpng')

%% Fit the data and plot.
y = 1./c;
xNew = linspace(min(x), max(x), 1000);
A = zeros(length(x), 2);
A(:, 1) = x.^(-1);
A(:, 2) = x.^(2);
At = transpose(A);
a = (At*A) \ (At*y);
Mx = a(1).*xNew.^(-1) + a(2).*xNew.^(2);
fig2 = figure(2);
set(gca, 'fontsize', 20);
plot(x, c, 'ro')
hold on
grid on
plot(xNew, 1./Mx, 'b-', 'LineWidth', 2)
xlabel('x')
ylabel('c')
legend('Original Data', '1/M_{2}(x)')
print(fig2, 'fittedDataPlot2', '-dpng')

% Residual analysis.
Mx2 = a(2).*x.^(-1) + a(2).*x.^(2);
r1 = norm(c - 1./Mx2, 2) / norm(c, 2)
format long
%a

%% Updated and improved model.
y = log(c./x);
A = zeros(length(x), 5);
for j = [1:5]
  A(:, j) = x.^(j-1);
end
At = transpose(A);
a = (At*A) \ (At*y);
a
xNew = linspace(min(x), max(x), 1000);
M5x = [];
for i = [1:length(xNew)]
  M5x(i) = sum(a.*xNew(i).^([0:4]'));
end
fig3 = figure(3);
set(gca, 'fontsize', 20);
plot(x, c, 'ro')
hold on
grid on
plot(xNew, 1./Mx, 'b-', 'LineWidth', 2);
plot(xNew, xNew.*exp(M5x), 'k-', 'LineWidth', 2);
xlabel('x')
ylabel('y')
legend('Original Data', '1/M_{2}(x)', 'xe^{M_{5}(x)}')
print(fig3, 'fittedDataPlot3', '-dpng')

% Residual analysis.
cSmall = [];
for i = [1:length(x)]
  cSmall(i) = sum(a.*x(i).^([0:4]'));
end
r2 = norm(c-x.*exp(cSmall'), 2) / norm(c, 2)
