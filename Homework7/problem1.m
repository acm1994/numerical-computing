% Aaron Christopher Montano
% Melissa Moreno
% MATH / CS 375: Homework 7 - Problem 1

%% Part (i): Read in data.
load data1.mat
%fig1 = figure;
%set(gca, 'fontsize', 20)
%plot(x, y, 'ro')
%xlabel('x')
%ylabel('y')
%grid on
%print(fig1, 'data1Plot', '-dpng')

%% Part (iii): Least squares data.
% Find the vector a.
A = ones(length(x), 3);
A(:, 2) = cos(x);
A(:, 3) = exp(-x);
At = transpose(A);
a = (At*A) \ (At*y);

% Plot fitted values.
xNew = linspace(min(x), max(x), 1000);
yNew = a(1) + a(2)*cos(xNew) + a(3)*exp(-xNew);
fig2 = figure;
set(gca, 'fontsize', 20)
plot(x, y, 'ro')
hold on
grid on
plot(xNew, yNew, 'b-', 'LineWidth', 2)
xlabel('x')
ylabel('y')
legend('Original Data', 'M_{3}(x)')
print(fig2, 'dataFittedPlot', '-dpng')

% Normalized residual analysis.
yNew2 = a(1) + a(2)*cos(x) + a(3)*exp(-x);
r = norm(y - yNew2, 2) / norm(y, 2)
