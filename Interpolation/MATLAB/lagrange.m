% Polynomial Interpolation (Langrange Method).
% param:
% xCoor - x-coordinates (row vector).
% yCoor - y-coordinates (row vector).
% NOTE: both input vectors must be the same dimension.
function c = lagrange(xCoor, yCoor)

  % Check to see if both vectors are of the same dimension.
  if length(xCoor) ~= length(yCoor)
    error('Langrange Method failure: input vectors must be the same dimension.')
  end

  % Compute Lagrange polynomials.
  syms x;
  li = ones(1, length(xCoor)); % Lagrange polynomials.
  for i = [1:length(xCoor)]
    for k = [1:length(xCoor)]
      % Check if xi and xk are the same.
      if xCoor(i) == xCoor(k)
        continue
      else
        li(i) = li(i) * (x - xCoor(k)) / (xCoor(i) - xCoor(k));
      end
    end
  end

  % Compute final polynomial. 
  c = sum(yCoor.*li);
      
end 
