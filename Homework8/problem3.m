% Aaron Christopher Montano
% Melissa Moreno
% 05 / 03 / 2018

% Define basic values.
f1 = @(x) 1./(1+x.^2);
f2 = @(x) exp(sin(4.*pi.*x));
a = -1; b = 1;
h = [1/2, 1/4, 1/8, 1/16, 1/32, 1/64];
n = 2./h;
Iexact1 = pi/2;
Iexact2 = trapezoidal(f2, a, b, 100000);

%% F1.
Iapprox1 = [];
index1 = 1;
for i = n
  [x1, w1] = Legendre_knots(i, [a, b]);
  Iapprox1(index1) = sum(w1.*f1(x1));
  index1 = index1 + 1;
end

%% F2.
Iapprox2 = [];
index2 = 1;
for j = n
  [x2, w2] = Legendre_knots(j, [a, b]);
  Iapprox2(index2) = sum(w2.*f2(x2));
  index2 = index2 + 1;
end

% F1 Plot.
fig1 = figure(1);
set(gca, 'fontsize', 20);
loglog(h, abs(Iapprox1 - Iexact1), 'r-x')
hold on
loglog(h, h.^2, 'b--')
loglog(h, h.^4, 'g--')
loglog(h, h.^6, 'm--')
xlabel('h')
ylabel('abs. err.')
legend('I_{approx}', 'h^{2}', 'h^{4}', 'h^{6}', 'location', 'southeast')
print(fig1, 'plot3', '-dpng')

% F2 Plot.
fig2 = figure(2);
set(gca, 'fontsize', 20);
loglog(h, abs(Iapprox2 - Iexact2), 'r-x')
hold on
loglog(h, (h.^2)*10000, 'b--')
loglog(h, (h.^4)*10000, 'g--')
loglog(h, (h.^6)*10000, 'm--')
xlabel('h')
ylabel('abs. err.')
legend('I_{approx}', 'h^{2}', 'h^{4}', 'h^{6}', 'location', 'southeast')
print(fig2, 'plot4', '-dpng')
