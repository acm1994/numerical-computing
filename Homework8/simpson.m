% Aaron Christopher Montano
% Melissa Moreno
% MATH / CS 375: Homework 8 - Simpson's Rule.
% simpson(f, a, b, m)
% Param:
% f - function handle.
% a - left bound.
% b - right bound.
% m - m+1 evenly-spaced gridpoints.
% NOTE: m must be an even integer.
function I = simpson(f, a, b, m)

  % Check to see if m is even.
  if mod(m, 2) == 1
    error('Simpsons Rule failure: m must be an even integer. ')
  end

  % Determine size of h and compute I.
  h = (b-a)/m;
  xi = [a:h:b];;
  I = (h/3)*(f(xi(1)) + f(xi(end)) + 4*sum(f(xi(2:2:end))) + 2*sum(f(xi(3:2:end-2))));

end 
