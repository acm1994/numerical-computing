import math
from scipy.integrate import quad

# Aaron Christopher Montano
# Melissa Moreno
# 04 / 30 / 2018
# MATH / CS 375: Homework 2 -- Problem 2.

# Function declaration.
def complexFunction(x):
    return math.exp(4*math.pi*x);

# Evaulate integral from -1 to 1.
I = quad(complexFunction, -1, 1)
print(I)
