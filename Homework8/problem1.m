% Aaron Christopher Montano
% Melissa Moreno
% CS / MATH 375 - Homework 8: Problem 1

% Define interval lengths, exact integral and function handle.
a = -1; b = 1;
h = [1/2, 1/4, 1/8, 1/16, 1/32, 1/64]; 
Iexact = pi/2;
f = @(x) 1./(1 + x.^2);

% Perform approximations.
i = 1;
IapproxM = 0; IapproxS = 0; IapproxT = 0;
for k = h
  m = (b-a)/k;
  IapproxM(i) = midpoint(f, a, b, m);
  IapproxS(i) = simpson(f, a, b, m);
  IapproxT(i) = trapezoidal(f, a, b, m);
  i = i + 1;
end

% Set up figure and perform approximations.
fig = figure(1);
set(gca, 'fontsize', 20);
xlabel('h')
ylabel('error')
grid on
loglog(h, abs(IapproxM - Iexact), 'g-*')
hold on
loglog(h, abs(IapproxS - Iexact), 'b-*')
loglog(h, abs(IapproxT - Iexact), 'm-*')
loglog(h, h.^2, 'r--')
loglog(h, (h.^4)./8, 'k--')
legend('Midpoint', 'Simpson', 'Trapezoidal', 'Location', 'southeast', 'h^{2}', 'h^{4}')
xlabel('h')
ylabel('abs. err.')
print(fig, 'problem1', '-dpng')
