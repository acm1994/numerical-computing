% Aaron Christopher Montano
% Melissa Moreno
% CS / MATH 375 - Homework 8: Problem 1

% Define interval lengths, exact integral and function handle.
a = -1; b = 1;
h = [1/2, 1/4, 1/8, 1/16, 1/32, 1/64]; 
f = @(x) exp(sin(4*pi*x));
Iexact = trapezoidal(f, a, b, 100000)

% Perform approximations.
i = 1;
IapproxM = 0; IapproxS = 0; IapproxT = 0;
for k = h
  m = (b-a)/k;
  IapproxM(i) = midpoint(f, a, b, m);
  IapproxS(i) = simpson(f, a, b, m);
  IapproxT(i) = trapezoidal(f, a, b, m);
  i = i + 1;
end

% Set up figure and perform approximations.
fig = figure(1);
set(gca, 'fontsize', 20);
xlabel('h')
ylabel('error')
grid on
loglog(h, abs(IapproxM - Iexact), 'g-*')
hold on
loglog(h, abs(IapproxS - Iexact), 'b-*')
loglog(h, abs(IapproxT - Iexact), 'm-*')
loglog(h, (h.^2)*2000, 'k--')
loglog(h, (h.^4)*2000, 'r--')
legend('Midpoint', 'Simpson', 'Trapezoidal', 'Location', 'southeast', 'h^{2}', 'h^{4}')
print(fig, 'problem2', '-dpng')
