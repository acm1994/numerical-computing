% Aaron Christopher Montano
% Melissa Moreno
% MATH / CS 375: Homework 8 - Midpoint.
% midpoint(f, a, b, m)
% Param:
% f - function handle.
% a - left bound.
% b - right bound.
% m - number of discretized points.
% NOTE: m must be greater than or equal to 2.
  function I = midpoint(f, a, b, m)

  % Determine h and the midpoints of our function.
  h = (b-a)./m;
  i = [0:m];
  initialPoints = a + i.*h;
  shift = 0.5*(initialPoints(2) - initialPoints(1));
  w = initialPoints(1:(end-1)) + shift;

  % Perform approximation.
  I = h*sum(f(w));
   
end 
