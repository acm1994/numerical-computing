% Aaron Christopher Montano
% Melissa Moreno
% MATH / CS 375: Homework 6 - Trapezoidal Rule
% trapezoidal(f, a, b, h)
% Param:
% f - function handle.
% a - left bound.
% b - right bound.
% m - number of discretization steps.
function I = trapzoidal(f, a, b, m)

  % Compute h and I.
  h = (b-a)/m;
  i = [1:(m-1)];
  xi = a + i.*h;
  I = 0.5*h*(f(a) + f(b) + 2*sum(f(xi)));

end
