% Jacobi Method.
% jacobi(A, b, n, tol)
% Param:
% A - n x n matrix (assumed to be sparse).
% b - solution to equation.
% n - limit to number of iterations.
% tol - error tolerance.
function x = jacobi(A, b, n, tol)

   % Initialize values.
   k = 0;
   xPrev = zeros(length(b), 1);
   xCurr = zeros(length(b), 1);
   Bj = -1*inv(diag(diag(A)))*(tril(A) + triu(A));
   c = inv(diag(diag(A)))*b;

   % Begin iterations.
   xCurr = Bj*xPrev + c;
   k = k + 1;
   while norm(xCurr - xPrev, 2) >= tol

     % Check to see if the number of iterations has gone past
     % our declared tolerance.
     if k > n
       error('Jacobi Method failure; divergence. ')
       break
     end
     
     % Perform calculations.
     xPrev = xCurr;
     xCurr = Bj*xPrev + c;
     k = k + 1;
   end

   % Report on values.
   if k <= n
     fprintf('Jacobi method convergence in %d iterations. \n', k)
     x = xCurr;
   end 

end
   
