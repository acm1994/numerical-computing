% Aaron Christopher Montano
% Melissa Moreno
% 05 / 03 / 2018
% MATH / CS 375: Homework 5
% Question 2.

% Define matrices.
A = [2, -1, 0, 0, 0, 0;
      -1, 2, -1, 0, 0, 0;
      0, -1, 2, -1, 0, 0;
      0, 0, -1, 2, -1, 0;
      0, 0, 0, -1, 2, -1;
      0, 0, 0, 0, -1, 2];
b = [0;
      2;
      3;
      -1;
      2;
      1];

diagA = diag(diag(A));
Aupper = triu(A);
Alower = tril(A);
diagAinv = inv(diagA);
AupperInv = inv(Aupper);
AlowerInv = inv(Alower);

% Bj and Bgs.
Bj = -1*diagAinv*(Alower + Aupper);
Bgs = -1*inv((eye(6) + diagAinv*Alower))*diagAinv*Aupper;

% Report eigenvalues.
fprintf('Spectral radius of Bj: %d \n', max(abs(eig(Bj))))
  fprintf('Spectral radius of Bgs: %d \n', max(abs(eig(Bgs))))

  % Jacobi Method will not converge for Bj.
  % Gauss Seidel Method WILL converge for Bgs.
