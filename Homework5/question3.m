% Aaron Christopher Montano
% Melissa Moreno
% 03 / 05 / 2018
% CS / MATH 375: Homework 5
% Question 3

% Define matrices.
A = [2, -1, 0, 0, 0, 0;
      -1, 2, -1, 0, 0, 0;
      0, -1, 2, -1, 0, 0;
      0, 0, -1, 2, -1, 0;
      0, 0, 0, -1, 2, -1;
      0, 0, 0, 0, -1, 2];
b = [0;
      2;
      3;
      -1;
      2;
      1];

% Perform Jacobi method.
n = 1000;
tol = 0.001;
jacobi(A, b, n, tol)

% Gauss-Seidel.
gaussSeidel(A, b, n, tol)
