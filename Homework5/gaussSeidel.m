% Gauss-Seidel Method.
% gaussSeidel(A, b, n, tol)
% Param:
% A - n x n matrix (sparse assumption).
% b - solution to system (column vector).
% n - limit to number of iterations.
% tol - error tolerance.
function x = gaussSeidel(A, b, n, tol)

   % Initialize variables.
   k = 0;
   xPrev = zeros(length(b), 1);
   xCurr = zeros(length(b), 1);
   Bgs = -1*inv((eye(length(b)) + inv(diag(diag(A)))*tril(A)))*inv(diag(diag(A)))*triu(A);
   c = inv(eye(length(b)) + inv(diag(diag(A)))*tril(A))*inv(diag(diag(A)))*b;

   % Begin iterations.
   xCurr = Bgs*xPrev + c;
   k = k + 1;
   while norm(xCurr - xPrev, 2) > tol
     % Check if we are past our iteration limit.
     if k > n
       fprintf('Gauss-Seidel Method failure; divergence. \n')
       break
     end

     % Perform calculation.
     xPrev = xCurr;
     xCurr = Bgs*xPrev + c;
     k = k + 1;
   end

   % Report.
   fprintf('Gauss-Seidel convergence in %d iterations: \n', k)
   x = xCurr;
   
end 
